#include <cstdio>
#include <cmath>
#include <vector>
#include <string>

class AbstractFunc
{
private:
    double _a;
    double _b;
    double _c;

public:
    AbstractFunc(double a, double b, double c)
    {
        _a = a;
        _b = b;
        _c = c;
    }

    double a() { return _a; }
    double b() { return _b; }
    double c() { return _c; }

    virtual double getFunctionValue(double x) = 0;
};

class Func1 : public AbstractFunc
{
public:
    Func1(double a, double b, double c) : AbstractFunc(a, b, c) {}
    double getFunctionValue(double x)
    {
        return a() * sin(b() * x);
    }
};

class Func2 : public AbstractFunc
{
public:
    Func2(double a, double b, double c) : AbstractFunc(a, b, c) {}
    double getFunctionValue(double x)
    {
        return a() * log(b() * x) + c();
    }
};

inline AbstractFunc * fakeFabric(int flag, double a, double b, double c)
{
    switch (flag)
    {
    case 1:  return new Func1(a, b, c);
    case 2:  return new Func2(a, b, c);
    default: return nullptr;
    }
}

std::vector<double> * byParamsLineFunc(std::vector<AbstractFunc*>& funcs, double x)
{
    std::vector<double> * y = new std::vector<double>();

    for (size_t i = 0; i < funcs.size(); ++i)
    {
        y->push_back(funcs[i]->getFunctionValue(x));
    }

    return y;
}

int main()
{
    puts("Choose func");
    int flag;
    scanf("%d", &flag);

    int n = 0;
    double x = 0;

    puts("Choose vector size");
    scanf("%d", &n);
    puts("Insert arg");
    scanf("%lf", &x);

    std::vector<AbstractFunc*> funcs = std::vector<AbstractFunc*>();
    double a, b, c;

    for (size_t i = 0; i < (size_t)n; ++i)
    {
        puts("Insert params a, b, c for func");
        scanf("%lf%lf%lf",
              &a,
              &b,
              &c
              );

        funcs.push_back(fakeFabric(flag, a, b, c));
    }

    std::vector<double> * y = byParamsLineFunc(funcs, x);

    if (y)
    {
        puts("Result:");
        for (size_t i = 0; i < (size_t)n; ++i)
        {
            printf("%lf\n", (*y)[i]);
        }
    }
    else
    {
        puts("Error!!!");
    }

    return 0;
}
